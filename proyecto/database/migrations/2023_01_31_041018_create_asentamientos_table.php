<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAsentamientosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('asentamientos', function (Blueprint $table) {
            $table->id();

            $table->string('name');
            $table->foreignId('zone_id');
            $table->foreign('zone_id')->references('id')->on('cat_zonas');
            $table->foreignId('settlement_type_id');
            $table->foreign('settlement_type_id')->references('id')->on('cat_tipoasentamientos');
            $table->foreignId('zip_code_id');
            $table->foreign('zip_code_id')->references('id')->on('cat_codigopostals');
            $table->foreignId('municipality_id');
            $table->foreign('municipality_id')->references('id')->on('cat_municipios');
            $table->integer('locality_id')->nullable();
            $table->softDeletes();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('asentamientos');
    }
}
