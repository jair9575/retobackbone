<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddLocalidadMunicipioEstadoToCatCodigopostalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cat_codigopostals', function (Blueprint $table) {

            $table->foreignId('federal_entity_id');
            $table->foreign('federal_entity_id')->references('id')->on('cat_estados');
            $table->foreignId('municipality_id');
            $table->foreign('municipality_id')->references('id')->on('cat_municipios');
            $table->integer('locality_id')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cat_codigopostals', function (Blueprint $table) {
            //
        });
    }
}
