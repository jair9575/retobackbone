<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
class Estado extends Model
{
    protected $hidden = [ 'id', 'deleted_at', 'updated_at', 'created_at' ];
    protected $fillable = [ 'name', 'code' ];
    protected $table = 'cat_estados';
    use HasFactory;
}