<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
class Codigopostal extends Model
{
    protected $hidden = [ 'id', 'federal_entity_id', 'municipality_id', 'locality_id', 'deleted_at', 'updated_at', 'created_at' ];
    protected $fillable = [ 'zip_code', 'federal_entity_id', 'municipality_id', 'locality_id' ];
    protected $table = 'cat_codigopostals';
    use HasFactory;

    // obtiene todos los asentamientos
    public function settlements()
    {
        return $this->hasMany(Asentamiento::class, 'zip_code_id', 'id')
        ->select(['key', 'name', 'zone_id','settlement_type_id', 'zip_code_id', 'asentamientos.id', 'cat_zonas.zone_type'])
        ->zone()->with('settlement_type');
    }
    // obtiene el estado
    public function federal_entity(){
        return $this->hasOne(Estado::class, 'id', 'federal_entity_id')
        ->select(['name', 'id as key', 'id', 'code']);
    }
    // obtiene el municipio
    public function municipality(){
        return $this->hasOne(Municipio::class, 'id', 'municipality_id')->select(['key', 'id', 'name']);
    }
    // escope para obtener la localidad 
    public function scopeLocality($query){
        $query->leftJoin('cat_localidads', 'cat_codigopostals.locality_id', '=', 'cat_localidads.id');
        return $query;
    }
}