<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
class Municipio extends Model
{
    protected $hidden = [ 'id', 'locality_id', 'deleted_at', 'updated_at', 'created_at'];
    protected $fillable = [ 'name','federal_entity_id', 'key' ];
    protected $table = 'cat_municipios';
    use HasFactory;
}