<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Zona extends Model
{
    protected $hidden = [ 'id', 'deleted_at', 'updated_at', 'created_at', ];
    protected $fillable = [ 'zone_type' ];
    protected $table = 'cat_zonas';
    use HasFactory;
}