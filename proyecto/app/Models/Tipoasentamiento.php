<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
class Tipoasentamiento extends Model
{
    protected $hidden = [ 'id', 'deleted_at', 'updated_at', 'created_at' ];
    protected $table = 'cat_tipoasentamientos';
    use HasFactory;
}