<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
class Asentamiento extends Model
{
    protected $hidden = [ 'id', 'settlement_type_id', 'zip_code_id', 'zone_id', 'municipality_id', 'locality_id', 'deleted_at', 'updated_at', 'created_at'];
    protected $fillable = [ 'key', 'name', 'zone_id', 'settlement_type_id', 'zip_code_id', 'municipality_id', 'locality_id'];
    use HasFactory;

    // obtiene el tipo de zona
    public function zone_type(){
        return $this->hasOne(Zona::class, 'id', 'zone_id')->select(['id', 'zone_type']);
    }
    // obtiene el tipo de asentamiento
    public function settlement_type(){
        return $this->hasOne(Tipoasentamiento::class, 'id', 'settlement_type_id');
    }

    //escope para obtener la zona
    public function scopeZone($query){
        $query->leftJoin('cat_zonas', 'asentamientos.zone_id', '=', 'cat_zonas.id');
        return $query;
    }
}