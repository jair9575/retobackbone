<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
class Localidad extends Model
{
    protected $hidden = [ 'id', 'locality_id', 'deleted_at', 'updated_at', 'created_at' ];
    protected $fillable = [ 'locality' ];
    protected $table = 'cat_localidads';
    use HasFactory;
}