<?php
namespace App\Http\Traits\ZipCode;
use App\Models\Codigopostal;
trait ListTrait {

    // lista los codigo postales
    public function getDataByZipCode($zip_code){
        if(is_numeric($zip_code)){
            return Codigopostal::select("cat_codigopostals.zip_code", "cat_codigopostals.id", "cat_codigopostals.federal_entity_id", "cat_codigopostals.municipality_id", "cat_localidads.locality")
            ->with('settlements')
            ->locality()
            ->with('federal_entity')
            ->with('municipality')
            ->where('zip_code',"=", $zip_code)
            ->first();
        }else{
            return response()->json(
                ['error' => 'El valor debe ser numérico'],
                404
            );
        }
    }
}