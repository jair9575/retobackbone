<?php
use Illuminate\Support\Facades\Route;

Route::prefix('api')->group(function() {
    Route::get('/zip-codes/{zip_code}', [App\Http\Controllers\CodigopostalController::class, 'getDataByZipCode'])
    ->name('getDataByZipCode');
});