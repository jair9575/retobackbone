<?php
use Illuminate\Support\Facades\Route;

require app_path('../routes/zip_code.php');

Route::get('/', function () {
    return view('welcome');
});
