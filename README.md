## Anotaciones
Primero analicé el excel para tener la BD mas clara y asi establecer catalogos y nombres de columnas.
Para guardar el excel usé el programa NAVICAT pero el excel en la columna c_estado incluía una comilla simple,
tuve que borrar formatos de todas las columnas, reuní todos los datos del excel en 1 sola hoja para 
posterior insertar todo el excel a una tabla llamada "excel_descargado"
para poder manipularla y hacer los inserts correspondientes, aqui los querys que usé:
<br />

    #inserta los estados
```html
    INSERT INTO cat_estados (id, name)
    SELECT UPPER(c_estado), d_estado from excel_descargado GROUP BY c_estado;
```
<br />

    #inserta los codigos postales
```html
    INSERT INTO cat_codigopostals (zip_code)
    SELECT d_codigo from excel_descargado GROUP BY d_codigo;
```
<br />

    #inserta los municipios
```html
    INSERT INTO cat_municipios (cat_municipios.name, federal_entity_id, cat_municipios.key)
    select UPPER(D_mnpio), c_estado, c_mnpio from excel_descargado GROUP BY D_mnpio;
```
<br />

    #inserta el catalogo de zonas
```html
    INSERT INTO cat_zonas (zone_type)
    select d_zona from excel_descargado GROUP BY d_zona;
```
<br />

    #inserta las localidades
```html
    INSERT INTO cat_localidads (locality, federal_entity_id)
    select UPPER(d_ciudad), c_estado from excel_descargado WHERE !ISNULL(d_ciudad) GROUP BY d_ciudad, c_estado;
```
<br />

    #inserta el tipo de asentamientos
```html
    INSERT INTO cat_tipoasentamientos (id, name)
    SELECT c_tipo_asenta, d_tipo_asenta from excel_descargado GROUP BY c_tipo_asenta;
```

<br />
para insertar en la tabla de asentamientos me aparecio el siguiente error:
<br />

```html
    llegal mix of collations (utf8mb4_unicode_ci,IMPLICIT) and (utf8mb4_general_ci,IMPLICIT) for operation '='
```
lo cual se solucionó con: 
<br />

```html
    ALTER TABLE excel_descargado CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
```

<br />

de esta manera inserté los datos en la tabla de asentamientos:

<br />

```html
    INSERT INTO asentamientos (asentamientos.key, asentamientos.`name`, asentamientos.zone_id, asentamientos.settlement_type_id, asentamientos.zip_code_id, asentamientos.municipality_id, asentamientos.locality_id)
    select id_asenta_cpcons, UPPER(d_asenta), cat_zonas.id as zone_id, c_tipo_asenta, cat_codigopostals.id as codigo_postal_id, cat_municipios.id as municipio_id, cat_localidads.id as localidad_id
    from excel_descargado
    join cat_zonas on zone_type = d_zona
    join cat_municipios on name = D_mnpio
    LEFT JOIN cat_localidads on locality = d_ciudad
    join cat_codigopostals on cat_codigopostals.zip_code = d_codigo;
```

<br />

Bien y referente al codigo, utilicé las Relationships Eloquent del propio laravel, tiene una validación para el parametro de "zip_code" debe ser solo numérico, tambien utilice algunos scopes para asi tener un código más limpio, por ultimo agregue una pantalla inicial y pegué parte de las instrucciones y un ejemplo a la RUTA.

<br />

## Notas
1.- Los tiempos de carga en local ronda desde 180 a 250 adjunto screenshot al proyecto "evidencia1_peticion.png"
<br />
![Alt text](/evidencia1_peticion.png?raw=true "Optional Title")

<br />

2.- El "index.php" se encuentra en raiz y el proyecto laravel en la carpeta "proyecto"

<br />

3.- La BD se encuentra en el archivo "retobackboneDB.sql"
